\book{Groups and Quotienting}

%%%
%%%
%%%

\chapter{Equivalence relations}

%%%
%%%
%%%

\begin{figure}[ht]
\centering
\answergraph{include/images/EqualsQuote720.png}
\caption{{\scshape The Founder} on equivalence relations.}
\label{fig:EqualsQuote}
\end{figure}

What constitutes exactly a ``defintion of equals''?

Let's start with someting called an \term{equivalence relation}.  A
\term{relation} on $t$ means a function $r$

\begin{lstlisting}[language=Erlang]
-spec r(t(), t()) -> 0 | 1.
\end{lstlisting}

The following three properties form what is called a
\term{equivalence relation}. Suppose we have a ``type'' (loosely
defined for now, will define precisely later) $t()$. An
\term{equivalence relation} $r$ is a relation where

\begin{description}
    \item[Reflexive property:]
        $r(A, A) = 1$ for all $A :: t()$.

        ``Everything is equal to itself.''
    \item[Symmetric property:]
        $r(A, B) = r(B, A)$ for all $A :: t()$ and $B :: t()$.

        ``If $A$ equals $B$ then $B$ equals $A$.''
    \item[Transitive property:]
        ($r(A, B)$ and $r(B, C)$) implies $r(A, C)$, for all $A, B, C
        :: t()$.

        ``If $A$ equals $B$ and $B$ equals $C$ then $A$ equals $C$.''

        (If you're unclear, see Revelations 14 for a white person
        definition of ``implies''.)
\end{description}

Our starting point for ``definition of equals'' is an equivalence
relation. Generally, a branch of math starts with a relation, that
happens to be an equivalence relation, and then the other auxilliary
properties of the relation are, in a sense, what defines the field.
You study what transformations preserve the equivalence relation, and
what transformations break the equivalence relation.

And, in a deeper sense, this pattern of ``factoring out an
equivalence relation'' is the most fundamental reasoning pattern in
math. It's really the driver concept behind everything else.

Let's focus our attention for a moment in a slightly different
direction. Let's instead think about \term{equivalence class}es,
which is a class of things that are equivalent with respect to an
equivalence relation.

Let me give some examples:

\begin{enumerate}
\item
    The equivalence relation in the field of algebraic topology is
    called \term{homotopic equivalence}. The coffee mug and the torus
    are in the same homotopy class. A cube and a 2D sphere are in the
    same homotopy class.\footnote{
            For now, keep as your fake definition of \term{homotopy
            equivalence}, as the generalization of ``we can get back
            and forth by means of a continuous path.''

            See
            \cref{fig:homotopy0,fig:homotopy1,fig:homotopy-nonexample}
            for visual motivation.

            For a proper definition, see \textcite[p. 3]{Hatcher}.
            Note that \term{homotopy equivalence} means something
            slightly stronger than \term{homotopy}.
        }

\item
    In mod-2 arithmetic, all even numbers are an equivalence class
    (equal 0 mod 2). Likewise, all odd numbers are an equivalence
    class (equal 1 mod 2). These equivalence classes are typically
    called \term{residue class}es.
\item
    In mod-3 arithmetic,
    \begin{enumerate}
        \item all things that equal 0 mod 3 are in a residue class
        \item all things that equal 1 mod 3 are in a residue class
        \item all things that equal 2 mod 3 are in a residue class
        \item the residue class of 3 is the same as that of 0
        \item and so on
    \end{enumerate}
\item
    Generally, in mod-$k$ arithmetic, each residue $r$ defines an
    equivalence class, namely things that are equal to $r$ mod $k$.
\end{enumerate}

\begin{figure}[h]
    \centering
    \inlinegraph{include/images/Homotopy_dim0.png}
    \caption{
            Homotopy idea in dimension 0.
            Note that \term{homotopy equivalence} means something
            slightly stronger than \term{homotopy}.
        }
    \label{fig:homotopy0}
\end{figure}

\begin{figure}[h]
    \centering
    \inlinegraph{include/images/Homotopy_dim1.png}
    \caption{
            Homotopy idea in dimension 1.
            Note that \term{homotopy equivalence} means something
            slightly stronger than \term{homotopy}.
        }
    \label{fig:homotopy1}
\end{figure}

\begin{figure}[h]
    \centering
    \inlinegraph{include/images/Homotopy_nonexample.png}
    \caption{
            Example of two paths that are \textbf{not} homotopic.
            This also means the two paths are not homotopy-equivalent.
        }
    \label{fig:homotopy-nonexample}
\end{figure}



%%%
%%%
%%%

\chapter{Getting an Equivalence Relation from a Group Action}

%%%
%%%
%%%

Our focus will now shift again. In practice, instead of being given
an equivalence relation, you are typically given a starting space,
and then a \term{group} of symmetries of the space, and you are to
construct a new space, which is the original space with the group of
symmetries ``factored out.'' (This is called ``quotienting''). This
will be made more precise later, and some visual diagrams will follow
shortly. But for now, when you think ``group'' think ``symmetries.''

The group laws mirror closely those of an equivalence relation.
Meaning, suppose a group is acting on a type. In our terminology, the
type where the group acts is called the \term{stage} (the group
\term{act}s on the \term{stage}). The \term{group action} has the
effect of \term{partitioning} the stage
into equivalence classes; specifically, two elements are equal
precisely when they are in the same \term{orbit} of the group action.

The point: everything is about the definition of equals, and in
practice group theory is how we change our definition of equals.
Group theory also typically provides the tools for studying what
happens when we do that.

Let me right now tell you the laws of a group, and then we'll look at
some examples of what I mean.

\section{The Group Laws}

The \term{orbit} of an element $T :: t()$ with respect to a group
action by $g()$ means ``all the places I can get to in the
stage\footnote{$t()$ is the stage, the place where the group acts}
using the symmetries of a group.''

By probably incorrect analogy, if you imagine you are a knight on an
empty chess board, then

\begin{enumerate}
\item The chess board is the stage
\item The knight-move is the \term{generator} for the group
\item The group is all possible sequences of knight-moves
\item An element of the group is a specific sequence of knight-moves
\item Your orbit is all squares that you can get to by a sequence of knight-moves
\end{enumerate}

Question 1: suppose you are a bishop that finds yourself situated on
a black square, in an otherwise empty chess board. What is your
orbit?

Question 2: When we construct a space by factoring out by a group
action, what that means precisely is that the quotient space is the
``set'' of distinct orbits. So, if we factor out the chess board by
the bishop's move, then what is the resulting quotient space, as a
set?

Let's suppose we have a type $t$. A \term{group} some other type $g$
of functions from $t$ to itself; specifically

\begin{lstlisting}[language=Erlang]
-type g() :: fun((t()) -> t()).
\end{lstlisting}

The group laws are that $g$ must also have the following properties:

\begin{enumerate}
    \item
        There is a function \lstinline|id_g :: g()| which
        transforms every object to itself.
\begin{lstlisting}[language=Erlang]
id_g(X) ->
    X.
\end{lstlisting}

        This corresponds to the reflexive property (``everything is
        equal to itself''; in our case, ``everything is in its own
        orbit'').
    \item
        Every function is invertible; that is, for every
        \lstinline|G :: g()|, there is some other function
        \lstinline|GInv :: g()|, with the properties that

        \begin{enumerate}
            \item \lstinline|G(GInv(T)) =:= T| for all \lstinline|T :: t()|,
            \item \lstinline|GInv(G(T)) =:= T| for all \lstinline|T :: t()|.
        \end{enumerate}

        This corresponds to the symmetric property (``if $A$ equals
        $B$ then $B$ equals $A$''; in our case, ``if $B$ is in the
        orbit of $A$ then $A$ is in the orbit of $B$'').
    \item
        The transformations are composable. Meaning if $G :: g()$ and
        $H :: g()$ then $G \circ H :: g()$.

        This corresponds to the transitive property (``if $A$ equals
        $B$ and $B$ equals $C$ then $A$ equals $C$''; in our case,
        ``if $B$ is in the orbit of $A$ and $C$ is in the orbit of
        $B$ then $C$ is also in the orbit of $A$'').
\end{enumerate}

\section{Some handwavy diagram examples}

% homotopy group action

% Z/2 group action

% Z/3 group action

% Z/12 group action

% Infinite cylinder group action

% Torus group action

% Mobius strip group action

% Klein bottle group action

% Link to a video where I tried to explain quotienting to him

\section{Some way too ambitious far away lookaheads into the future}

% Universal covering theorem

% Noether's theorem

\section{Some less ambitious lookaheads to stuff we will actually get to}

%%%
%%%
%%%

\chapter{Physics Application: solving circuits}

\chapter{Physics Application: solving circuits}

%%%
%%%
%%%

\begin{figure}[ht]
    \centering
    \answergraph{include/images/LazyEval.png}
    \caption{Craig Cannabis tells us about physics.}
    \label{fig:CraigCannabis}
\end{figure}
