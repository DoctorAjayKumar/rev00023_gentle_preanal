% File: complex4_lex.xrl

Definitions.

WHITESPACE = [\s\t\n\r]

Rules.

's              : {token, {possessive_particle, TokenLine, TokenChars}}.
admire          : {token, {verb, TokenLine, TokenChars}}.
and             : {token, {conjunction, TokenLine, TokenChars}}.
divine          : {token, {adjective, TokenLine, TokenChars}}.
founder         : {token, {noun, TokenLine, TokenChars}}.
ever            : {token, {adverb, TokenLine, TokenChars}}.
everybody       : {token, {noun, TokenLine, TokenChars}}.
he              : {token, {noun, TokenLine, TokenChars}}.
humility        : {token, {noun, TokenLine, TokenChars}}.
i               : {token, {noun, TokenLine, TokenChars}}.
intellect       : {token, {noun, TokenLine, TokenChars}}.
is              : {token, {verb, TokenLine, TokenChars}}.
know            : {token, {verb, TokenLine, TokenChars}}.
lived           : {token, {verb, TokenLine, TokenChars}}.
love            : {token, {verb, TokenLine, TokenChars}}.
mathematician   : {token, {noun, TokenLine, TokenChars}}.
of              : {token, {preposition, TokenLine, TokenChars}}.
poops           : {token, {verb, TokenLine, TokenChars}}.
praise          : {token, {verb, TokenLine, TokenChars}}.
the             : {token, {determiner, TokenLine, TokenChars}}.
smartest        : {token, {superlative, TokenLine, TokenChars}}.
who             : {token, {preposition, TokenLine, TokenChars}}.

{WHITESPACE}+   : skip_token.

Erlang code.
