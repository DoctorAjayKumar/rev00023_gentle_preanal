% File: simple3_parse.yrl

Nonterminals sentence noun_phrase verb_phrase.
Terminals noun verb.
Rootsymbol sentence.

sentence    -> noun_phrase verb_phrase : pf_tree({stem, sentence, ['$1', '$2']}).
noun_phrase -> noun                    : {stem, noun_phrase, [{leaf, '$1'}]}.
verb_phrase -> verb                    : {stem, verb_phrase, [{leaf, '$1'}]}.

Erlang code.

% pretty format the whole tree (generate a deeplist of chars which we
% will flatten out in the shell)

% If it's a stem, make a sexp (StemType SubTree1 SubTree2 SubTree3 ...)
%
% The pf_subtrees/1 function handles putting spaces between the
% different trees
pf_tree({stem, StemType, SubTrees}) ->
    [$(, erlang:atom_to_list(StemType), pf_subtrees(SubTrees), $)];
pf_tree({leaf, {PartOfSpeech, _LineNumber, Word}}) ->
    [$(, erlang:atom_to_list(PartOfSpeech), " ", $", Word, $", $)].

% pretty format a list of subtrees, and handle putting spaces before
% them
pf_subtrees([]) ->
    "";
pf_subtrees([Tree | Rest]) ->
    [" ", pf_tree(Tree), pf_subtrees(Rest)].
